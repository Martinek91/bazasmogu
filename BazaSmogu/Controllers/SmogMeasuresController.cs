﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BazaSmogu.Models;

namespace BazaSmogu.Controllers
{
    public class SmogMeasuresController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SmogMeasures
        public ActionResult Index()
        {
            var measures = from s in db.SmogMeasures orderby s.MeasureDateTime select s;
            return View(measures);
            //return View(db.SmogMeasures.ToList());

        }

        // GET: SmogMeasures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            if (smogMeasure == null)
            {
                return HttpNotFound();
            }
            return View(smogMeasure);
        }

        // GET: SmogMeasures/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: SmogMeasures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PM10,PM25,Pressure,Temperature,MeasureDateTime")] SmogMeasure smogMeasure)
        {
            if (ModelState.IsValid)
            {
                db.SmogMeasures.Add(smogMeasure);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(smogMeasure);
        }

        // GET: SmogMeasures/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            if (smogMeasure == null)
            {
                return HttpNotFound();
            }
            return View(smogMeasure);
        }

        // POST: SmogMeasures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PM10,PM25,Pressure,Temperature,MeasureDateTime")] SmogMeasure smogMeasure)
        {
            if (ModelState.IsValid)
            {
                db.Entry(smogMeasure).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(smogMeasure);
        }

        // GET: SmogMeasures/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            if (smogMeasure == null)
            {
                return HttpNotFound();
            }
            return View(smogMeasure);
        }

        // POST: SmogMeasures/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            db.SmogMeasures.Remove(smogMeasure);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
