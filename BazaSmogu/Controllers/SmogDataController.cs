﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BazaSmogu.Models;

namespace BazaSmogu.Controllers
{
    
    public class SmogDataController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/SmogData
        public IQueryable<SmogMeasure> GetSmogMeasures()
        {
            return db.SmogMeasures;
        }

        // GET: api/SmogData/5
        [ResponseType(typeof(SmogMeasure))]
        public IHttpActionResult GetSmogMeasure(int id)
        {
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            if (smogMeasure == null)
            {
                return NotFound();
            }

            return Ok(smogMeasure);
        }

        // PUT: api/SmogData/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSmogMeasure(int id, SmogMeasure smogMeasure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != smogMeasure.Id)
            {
                return BadRequest();
            }

            db.Entry(smogMeasure).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SmogMeasureExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SmogData
        [ResponseType(typeof(SmogMeasure))]
        public IHttpActionResult PostSmogMeasure(SmogMeasure smogMeasure)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SmogMeasures.Add(smogMeasure);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = smogMeasure.Id }, smogMeasure);
        }

        // DELETE: api/SmogData/5
        [ResponseType(typeof(SmogMeasure))]
        public IHttpActionResult DeleteSmogMeasure(int id)
        {
            SmogMeasure smogMeasure = db.SmogMeasures.Find(id);
            if (smogMeasure == null)
            {
                return NotFound();
            }

            db.SmogMeasures.Remove(smogMeasure);
            db.SaveChanges();

            return Ok(smogMeasure);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SmogMeasureExists(int id)
        {
            return db.SmogMeasures.Count(e => e.Id == id) > 0;
        }
    }
}