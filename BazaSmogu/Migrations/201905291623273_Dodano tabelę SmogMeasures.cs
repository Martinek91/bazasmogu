namespace BazaSmogu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodanotabelęSmogMeasures : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SmogMeasures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PM10 = c.Double(nullable: false),
                        PM25 = c.Double(nullable: false),
                        Pressure = c.Double(nullable: false),
                        Temperature = c.Double(nullable: false),
                        MeasureDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SmogMeasures");
        }
    }
}
