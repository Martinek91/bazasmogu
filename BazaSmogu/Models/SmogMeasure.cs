﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BazaSmogu.Models
{
    public class SmogMeasure
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Wartość wymagana")]
        [DataType(DataType.Text)]
        public double PM10 { get; set; }

        [Required(ErrorMessage = "Wartość wymagana")]
        public double PM25 { get; set; }

        [Required(ErrorMessage = "Wartość wymagana")]
        public double Pressure { get; set; }

        [Required(ErrorMessage = "Wartość wymagana")]
        public double Temperature { get; set; }

        [Required(ErrorMessage = "Wartość wymagana")]
        [DataType(DataType.Date)]
        [Display(Name ="Measure Date")]
        public DateTime MeasureDateTime { get; set; }

    }
}