﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BazaSmogu.Startup))]
namespace BazaSmogu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
